# Checkpoint/ Restore

# Prequisite

* Docker: 20.10.25+dfsg1

* CRiU: 3.17.1

## Notes

Debian **trixie (testing)** doesn't contain the package CRiU, but [sid](https://packages.debian.org/sid/criu). A temporary solution is to download and install the sid package.

# Configuration

## Daemon

* Edit `/etc/docker/daemon.json`

    ```json
    {"experimental": true, "storage-driver": "vfs"}
    ```

* Restart docker service: `systemctl restart docker`

### Caution

When restarting docker service, it may remove all images in docker registory.

## Client

* Edit `~/.docker/config.json`

    ```json
    {"experimental": "enabled"}

# Steps 

1. Create a snapshot

    * `docker checkpoint create --leave-running=true <container id> <checkpointed name>`

    * The snapshot file is saved to `/var/lib/docker/containers/<container full id>/checkpoints/<checkpointed name>`

2. Copy the checkpointed dir to the destination host dir

    * Restore container locally

      * `cp -r /var/lib/docker/containers/<container full id>/checkpoints /path/to/dest/dir`

      * `docker stop <container full id> && docker rm <container full id>`

    * Restore container in a remote host

      * `scp -r <user>@<src host>:/var/lib/docker/containers/<container full id>/checkpoints/<checkpointed name> /path/to/dest/dir`

3. Create a new docker container in the destination host

    * `new_container_id=$(docker create <image name>)`

    * (Option) Copy the backup checkpoint in the previous step to the remote host

      * `scp -r /path/to/dest/dir/<checkpointed name> <user>@<dest host>:/var/lib/docker/containers/<new container full id>/checkpoints/<checkpointed name>`

4. Restore the container

    * `docker start --checkpoint <checkpointed name> <container id>`

    * --checkpoint-dir parameter is not yet supported. See [Restoring containers from a custom checkpoint-dir is broken](https://github.com/moby/moby/issues/37344)

# An Example Command

* Build docker

  * `docker build -t <image name>`

* Start container

  * `docker run -d <image name>`

* Restore container locally

  * `bin/docker-migrate -i <conainer full id> -c <checkpoint name> -d "/path/to/dest/dir" -m <image name>`

# References 

[1]. [Enabling daemon experimental features](https://docs.docker.com/reference/cli/dockerd/#description)

[2]. [Podman Checkpointing with Volume Restore: A Step-by-Step Guide](https://medium.com/@shubham.jadhav_18910/podman-checkpointing-with-volume-restore-a-step-by-step-guide-d5960a182940)

[3]. [Docker Checkpoint & Restore on another host?](https://forums.docker.com/t/docker-checkpoint-restore-on-another-host/27427/2)

[4]. [Docker CRiU](https://criu.org/Docker)
